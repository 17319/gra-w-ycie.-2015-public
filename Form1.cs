﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _14_Gra_w_zycie
{
    public partial class Okno : Form
    {
        int rozmiarPola = 20;
        Plansza p;
        private Int32 kroki;
        Wizualizacja wizualizacja;
        Wizualizacja wizualizacja2;
        public Okno()
        {
            InitializeComponent();
            kroki = 0;
            p = new Plansza(15, 20);
            
            p.zmienZycie(2, 2);
            p.zmienZycie(2, 3);
            p.zmienZycie(3, 3);
            p.zmienZycie(3, 4);

            p.zmienZycie(10, 10);
            p.zmienZycie(10, 11);
            p.zmienZycie(11, 10);
            p.zmienZycie(11, 11);


            p.zmienZycie(11, 3);
            p.zmienZycie(12, 3);
            p.zmienZycie(13, 3);

            p.zmienZycie(2, 13);
            p.zmienZycie(3, 13);
            p.zmienZycie(4, 13);
            p.zmienZycie(4, 12);
            p.zmienZycie(3, 11);
            

            wizualizacja = new WizualizacjaGraficzna(p,pbPlansza, rozmiarPola);
            wizualizacja2 = new WizualizacjaTekstowa(p, rtbPlansza);

            //pierwsze wyrysowanie całej planszy, później tylko odrysowywanie pojedynczych pól
            odrysuj();
            trackBarSzybkosc_Scroll(null, null);
        }

        private void odrysuj()
        {
            wizualizacja.Odrysuj();
            wizualizacja2.Odrysuj();
        }



        private void timer_Tick(object sender, EventArgs e)
        {
            kroki++;
            LabelKroki.Text = "Kroki=" + kroki;
            p.Graj();
            odrysuj();
        }

        private void trackBarSzybkosc_Scroll(object sender, EventArgs e)
        {
            timer.Interval = trackBarSzybkosc.Value;
        }

        private void buttonStartStop_Click(object sender, EventArgs e)
        {
            if (buttonStartStop.Text == "Start")
            {
                timer.Start();
                buttonStartStop.Text = "Stop";
            }
            else
            {
                timer.Stop();
                buttonStartStop.Text = "Start";
            }

        }

        private void pbPlanszaDuza_MouseUp(object sender, MouseEventArgs e)
        {
            p.zmienZycie(e.X / rozmiarPola, e.Y / rozmiarPola);
            odrysuj();
        }
    }
}
