﻿namespace _14_Gra_w_zycie
{
    partial class Okno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LabelKroki = new System.Windows.Forms.Label();
            this.pbPlansza = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.trackBarSzybkosc = new System.Windows.Forms.TrackBar();
            this.buttonStartStop = new System.Windows.Forms.Button();
            this.rtbPlansza = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlansza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSzybkosc)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelKroki
            // 
            this.LabelKroki.AutoSize = true;
            this.LabelKroki.Location = new System.Drawing.Point(206, 9);
            this.LabelKroki.Name = "LabelKroki";
            this.LabelKroki.Size = new System.Drawing.Size(13, 13);
            this.LabelKroki.TabIndex = 0;
            this.LabelKroki.Text = "0";
            // 
            // pbPlansza
            // 
            this.pbPlansza.Location = new System.Drawing.Point(15, 53);
            this.pbPlansza.Margin = new System.Windows.Forms.Padding(15);
            this.pbPlansza.Name = "pbPlansza";
            this.pbPlansza.Size = new System.Drawing.Size(310, 396);
            this.pbPlansza.TabIndex = 1;
            this.pbPlansza.TabStop = false;
            this.pbPlansza.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbPlanszaDuza_MouseUp);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // trackBarSzybkosc
            // 
            this.trackBarSzybkosc.Location = new System.Drawing.Point(15, 9);
            this.trackBarSzybkosc.Maximum = 1000;
            this.trackBarSzybkosc.Minimum = 10;
            this.trackBarSzybkosc.Name = "trackBarSzybkosc";
            this.trackBarSzybkosc.Size = new System.Drawing.Size(104, 45);
            this.trackBarSzybkosc.TabIndex = 2;
            this.trackBarSzybkosc.Value = 500;
            this.trackBarSzybkosc.Scroll += new System.EventHandler(this.trackBarSzybkosc_Scroll);
            // 
            // buttonStartStop
            // 
            this.buttonStartStop.Location = new System.Drawing.Point(125, 9);
            this.buttonStartStop.Name = "buttonStartStop";
            this.buttonStartStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStartStop.TabIndex = 3;
            this.buttonStartStop.Text = "Start";
            this.buttonStartStop.UseVisualStyleBackColor = true;
            this.buttonStartStop.Click += new System.EventHandler(this.buttonStartStop_Click);
            // 
            // rtbPlansza
            // 
            this.rtbPlansza.Font = new System.Drawing.Font("Courier New", 13F, System.Drawing.FontStyle.Bold);
            this.rtbPlansza.Location = new System.Drawing.Point(343, 53);
            this.rtbPlansza.Margin = new System.Windows.Forms.Padding(15);
            this.rtbPlansza.Name = "rtbPlansza";
            this.rtbPlansza.Size = new System.Drawing.Size(305, 396);
            this.rtbPlansza.TabIndex = 4;
            this.rtbPlansza.Text = "";
            // 
            // Okno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1168, 570);
            this.Controls.Add(this.rtbPlansza);
            this.Controls.Add(this.buttonStartStop);
            this.Controls.Add(this.trackBarSzybkosc);
            this.Controls.Add(this.pbPlansza);
            this.Controls.Add(this.LabelKroki);
            this.Name = "Okno";
            this.Text = "Gra w życie";
            ((System.ComponentModel.ISupportInitialize)(this.pbPlansza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSzybkosc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelKroki;
        private System.Windows.Forms.PictureBox pbPlansza;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TrackBar trackBarSzybkosc;
        private System.Windows.Forms.Button buttonStartStop;
        private System.Windows.Forms.RichTextBox rtbPlansza;
    }
}

