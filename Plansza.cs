﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14_Gra_w_zycie
{
    class Plansza
    {
        private Boolean[,] dane;

        public Int32 RozmiarX
        {
            get { return dane.GetLength(0); }
        }
        public Int32 RozmiarY
        {
            get { return dane.GetLength(1); }
        }
        public Boolean czyZywa(int x, int y)
        {
            return dane[x, y];
        }
        public Plansza(int rozmiarX, int rozmiarY)
        {
            dane = new Boolean[rozmiarX, rozmiarY];
            for (int i = 0; i < rozmiarX; i++)
            {
                for (int j = 0; j < rozmiarY; j++)
                {
                    dane[i, j] = false;
                }
            }
        }

        public void Graj()
        {
            Boolean[,] dane_stare = dane.Clone() as Boolean[,];
            int ile;
            for (int i = 0; i < RozmiarX; i++)
            {
                for (int j = 0; j < RozmiarY; j++)
                {
                    ile = 0;
                    for (int ii = Math.Max(i - 1,0); ii < Math.Min(i + 2,RozmiarX); ii++)
                    {
                        for (int jj = Math.Max(j - 1, 0); jj < Math.Min(j + 2, RozmiarY); jj++)
                        {
                            if ((i!=ii||j!=jj) && dane_stare[ii, jj] == true)
                            {
                                ile++;
                            }
                        }
                    }
                    //jeśli martwa
                    if (dane_stare[i,j]==false && ile == 3)
                    {
                        dane[i, j] = true;
                    }
                    //jeśli żywa
                    else if (dane_stare[i, j] == true && (ile == 2 || ile == 3))
                    {
                        dane[i, j] = true;
                    }
                    else
                    {
                        dane[i, j] = false;
                    }
                }
            }
        }

        internal void zmienZycie(int x, int y)
        {
            dane[x, y] = !dane[x, y];
        }
    }
}
