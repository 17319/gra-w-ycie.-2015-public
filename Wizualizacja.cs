﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _14_Gra_w_zycie
{
    abstract class Wizualizacja
    {
        protected Plansza plansza;
        public Wizualizacja(Plansza plansza)
        {
            this.plansza = plansza;
        }


        abstract public void Odrysuj();
    }
    class WizualizacjaGraficzna:Wizualizacja
    {
        private PictureBox obrazek;
        private Int32 rozmiar;
        private Graphics grafika;
        public WizualizacjaGraficzna(Plansza plansza, PictureBox obrazek, Int32 rozmiar)
            : base(plansza)
        {
            this.obrazek = obrazek;
            this.rozmiar = rozmiar;

            obrazek.Size = new Size(plansza.RozmiarX * rozmiar, plansza.RozmiarY * rozmiar);
            obrazek.Image = new Bitmap(plansza.RozmiarX * rozmiar, plansza.RozmiarY * rozmiar);
            grafika = Graphics.FromImage(obrazek.Image);
        }


        override public void Odrysuj()
        {
            Color c;
            for (int i = 0; i < plansza.RozmiarX; i++)
            {
                for (int j = 0; j < plansza.RozmiarY; j++)
                {
                    if (plansza.czyZywa(i, j))
                    {
                        c = Color.Black;
                    }
                    else
                    {
                        c = Color.White;
                    }
                    grafika.FillEllipse(new SolidBrush(c), i * rozmiar, j * rozmiar, rozmiar, rozmiar);
                }
            }
            obrazek.Refresh();
        }
    }
    class WizualizacjaTekstowa: Wizualizacja
    {
        private RichTextBox tekst;

        public WizualizacjaTekstowa(Plansza plansza, RichTextBox tekst)
            : base(plansza)
        {
            this.tekst = tekst;
            this.tekst.ScrollBars = RichTextBoxScrollBars.None;
        }


        override public void Odrysuj()
        {
            tekst.Text = "";
            for (int j = 0; j < plansza.RozmiarY; j++)
            {
                for (int i = 0; i < plansza.RozmiarX; i++) 
                {
                    if (plansza.czyZywa(i, j))
                    {
                        tekst.AppendText("*");
                    }
                    else
                    {
                        tekst.AppendText(" ");
                    }
                }
                tekst.AppendText("\n");
            }
        }
    }
}
